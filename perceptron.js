class Perceptron {

    constructor(inputs,weights,n,bias,biasw){
        this.inputs = inputs;
        this.weights = weights;
        this.n = n;
        this.bias = bias;
        this.biasw = biasw;
    }

    train(inputs,d){
        this.inputs = inputs;
        let weights = this.weights;
        
        inputs.push(this.bias);
        weights.push(this.biasw);

        console.log("Inputs: ",inputs);
        console.log("Weights: ",weights);

        let e = this.multiply(inputs,weights);
        let sum = this.sum(e);
        let y = this.step(sum);

        console.log("Multiplicacao: ", e, "Soma: " + sum);

        if(y === d){
            console.log("Acerto:" +  y);
            return true;
        }else{
            console.log("Erro:" +  y);
            this.correct(weights,d,y);
            console.log("Pesos corrigidos:", this.weights);
            return false;
        }
    }

    step(x){
        return (x > 0) ? 1 : 0;
    }

    correct(weights,d,y){
        weights = weights.map((w,i) =>{
            return w + (this.n * (d - y) * this.inputs[i]);
        }).filter(w => { return !Number.isNaN(w) });

        this.biasw = weights.splice(-1,1)[0];
        this.weights = weights;
    }

    sum(x){
        let sum = 0;
        x.forEach(z => { sum += z });
        return sum;
    }
    
    multiply(x,y){ return x.map((z,i) => {return z * y[i] }); }
}

class Treinador {

    constructor(inputs,weights,results,n,bias,biasw){
        this.inputs = inputs;
        this.weights = weights;
        this.results = results;
        this.n = n;
        this.bias = bias;
        this.biasw = biasw;
        this.perceptron = new Perceptron([],this.weights,this.n,this.bias,this.biasw);
        this.epoca = 1;
        this.amostra = 1;
    }

    execute(){
        let finish = false;
        while(!finish){
            finish = true;
            console.log("")
            console.log("------------------------------------------------------------------------------------")
            console.log("Época - " + this.epoca);
            console.log("------------------------------------------------------------------------------------")
            this.epoca++;
            this.amostra = 1;
            this.inputs.forEach((input,i) =>{
                console.log("Amostra - " + this.amostra);
                this.amostra++;
                if(!this.perceptron.train(input,this.results[i])) finish = false;
                console.log("---------------------------------------------------------")
            });
            this.weights = this.perceptron.weights;
            this.biasw = this.perceptron.biasw;
            this.perceptron = new Perceptron([],this.weights,this.n,this.bias,this.biasw);
        }
    }


}

treinador = new Treinador(
    [[1,1], //Entradas
     [0.5, 0],
     [0.6, 0],
     [0.8, 1],
     [0.9, 0]],
    [0.7,-0.1], //Pesos
    [1,0,0,1,1], //Resultados
    0.3, //Fator de aprendizagem
    -1, //Bias
    0.2 //Peso do bias
);


treinador.execute();